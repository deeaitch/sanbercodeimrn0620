//soal no 1
var nama = "John";
var peran = "werewolf";
if (nama == "") {
    console.log("Nama harus diisi!");
} else if (peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
} else {
    if (peran == "penyihir") {
        console.log("Selamat datang di Dunia Werewolf, " + nama + "\nHalo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (peran == "guard") {
        console.log("Selamat datang di Dunia Werewolf, " + nama + "\nHalo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (peran == "werewolf") {
        console.log("Selamat datang di Dunia Werewolf, " + nama + "\nHalo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
    } else {
        console.log("Selamat datang di Dunia Werewolf, " + nama + "\nPeran yang dimasukkan salah");
    }
}
console.log("\n");

//soal no 2
var tanggal = 3;
var bulan = 11;
var tahun = 1945;

switch (bulan) {
    case 1: {
        console.log(tanggal + " Januari " + tahun);
        break;
    }
    case 2: {
        console.log(tanggal + " Februari " + tahun);
        break;
    }
    case 3: {
        console.log(tanggal + " Maret " + tahun);
        break;
    }
    case 4: {
        console.log(tanggal + " April " + tahun);
        break;
    }
    case 5: {
        console.log(tanggal + " Mei " + tahun);
        break;
    }
    case 6: {
        console.log(tanggal + " Juni " + tahun);
        break;
    }
    case 7: {
        console.log(tanggal + " Juli " + tahun);
        break;
    }
    case 8: {
        console.log(tanggal + " Agustus " + tahun);
        break;
    }
    case 9: {
        console.log(tanggal + " September " + tahun);
        break;
    }
    case 10: {
        console.log(tanggal + " Oktober " + tahun);
        break;
    }
    case 11: {
        console.log(tanggal + " November " + tahun);
        break;
    }
    case 12: {
        console.log(tanggal + " Desember " + tahun);
        break;
    }
    default: {
        console.log("Tidak ada bulan dengan angka " + bulan);
    }
}