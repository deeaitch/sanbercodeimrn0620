class Score {
    constructor(subject, points, email) {
        this.subject = subject;
        this.points = points;
        this.email = email;
    }
    get subject() {
        return this._subject;
    }
    set subject(x) {
        this._subject = x;
    }
    get email() {
        return this._email;
    }
    set email(x) {
        this._email = x;
    }
    get points() {
        return this._points;
    }
    set points(x) {
        this._points = x;
    }
    average() {
        let average;
        if (this.points.length > 1) {
            let sumtotal = 0;
            for (let i = 0; i < this.points.length; i++) {
                sumtotal += this.points[i];
            }
            average = sumtotal / this.points.length;

        } else {
            average = this.points;
        }
        return average.toFixed(2);;

    }
}


//soal no 2
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
    let email, scores, datano;
    switch (subject) {
        case "quiz-1": {
            datano = 1;
            break;
        }
        case "quiz-2": {
            datano = 2;
            break;
        }
        case "quiz-3": {
            datano = 3;
            break;
        }
        default: {
            datano = 1;
            break;
        }

    }
    let dataarray = [];
    for (let i = 1; i < data.length; i++) {
        email = data[i][0];
        scores = data[i][datano];
        let returndata = new Score(subject, scores, email);
        dataarray.push(returndata);

        //const theString = `${returndata.subject} ${returndata.email}, ${returndata.points}`;

    }
    console.log(dataarray);

}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//soal no 3
function recapScores(data) {
    let score = [];
    let dataprint = [];
    let theString;
    for (let i = 1; i < data.length; i++) {
        for (let j = 1; j < data[i].length; j++) {
            score.push(data[i][j]);
        }
        let result = new Score("", score, data[i][0]);
        let averages = result.average();
        if (averages > 90) {
            theString = `${i}. Email: ${data[i][0]} \n Rata-rata: ${averages} \n Predikat : honour \n`;
        } else if (averages < 90 && averages > 80) {
            theString = `${i}. Email: ${data[i][0]} \n Rata-rata: ${averages} \n Predikat : graduate \n`;
        } else if (averages < 80 && averages > 70) {
            theString = `${i}. Email: ${data[i][0]} \n Rata-rata: ${averages} \n Predikat : participant \n`;
        } else {
            theString = `${i}. Email: ${data[i][0]} \n Rata-rata: ${averages} \n Predikat : fails \n`;
        }
        console.log(theString);
    }
}

recapScores(data);