import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";

//import Component from "./Latihan/Component/Component";
import YoutubeUI from "./tugas/Tugas12/App";
import Component from "./Latihan/Component/index";
import Login from "./tugas/Tugas13/Login";
import About from "./tugas/Tugas13/About";
import Register from "./tugas/Tugas13/Register";

export default function App() {
  // const App = () => {
  return (
    //<YoutubeUI />
    //<Component />
    //<Login />
    //<Register />
    <About />
    // <View style={styles.container}>
    //   <Text> Open up App.js to start working on your app! </Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

// export default App;
