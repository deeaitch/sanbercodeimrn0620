//soal no 1 release 0
class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }

    get name() {
        return this._name;
    }
    set name(x) {
        this._name = x;
    }
    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


//soal no 1 release 1
class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2;
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2;
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
// console.log(sungokong.name) // "kera sakti"
// console.log(sungokong.legs) // 2
// console.log(sungokong.cold_blooded) // false
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



//Soal no 2
class Clock {
    constructor({
        template
    }) {
        this.template = template;
        var timer;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        this.template = hours + ":" + mins + ":" + secs;
        //var output = this.template;

        console.log(this.template);
        //console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(this.render, 1000);
    }
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();