//soal no 1
var num = 2;
console.log("Looping Pertama");
while (num <= 20) {
    console.log(num + " - I love coding");
    num += 2;
}

var num = 20;
console.log("Looping Kedua");
while (num >= 2) {
    console.log(num + " - I love coding");
    num -= 2;
}

//soal no 2
console.log("Soal No 2");
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 1) {
        if (i % 3 == 0) {
            console.log(i + " I love coding");
        } else {
            console.log(i + " Santai");
        }
    } else {
        console.log(i + " Berkualitas");
    }
}

//soal no 3
console.log("Soal No 3");
var char = "";
for (var i = 1; i <= 4; i++) {
    for (var j = 1; j <= 8; j++) {
        char += "#";
    }
    char += "\n";
}
console.log(char);


//soal no 4
console.log("Soal no 4");
var char2 = "";
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j <= i; j++) {
        char2 += "#";
    }
    char2 += "\n";
}
console.log(char2);

//soal no 5
console.log("Soal no 5");
var char3 = "";
for (var i = 1; i <= 8; i++) {
    for (var j = 1; j <= 8; j++) {
        if (i % 2 == 1) {
            if (j % 2 == 1)
                char3 += " ";
            else
                char3 += "#";
        } else {
            if (j % 2 == 0)
                char3 += " ";
            else
                char3 += "#";
        }
    }
    char3 += "\n";
}
console.log(char3);