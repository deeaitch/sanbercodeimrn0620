//Soal No 1
function arrayToObject(arr) {
    var person = {};
    if (arr.length < 1)
        console.log("");
    else {
        for (var i = 0; i < arr.length; i++) {
            person.firstName = arr[i][0];
            person.lastName = arr[i][1];
            person.gender = arr[i][2];
            var now = new Date();
            var thisYear = now.getFullYear();
            if (typeof arr[i][3] == "undefined" || arr[i][3] == "" || arr[i][3] > thisYear) {
                person.age = "Invalid Birth Year";
            } else {
                person.age = thisYear - arr[i][3];
            }
            console.log(i + 1 + ". " + person.firstName + " " + person.lastName + ": ", person);
        }
    }
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""


//Soal No 2
function shoppingTime(memberId, money) {
    if (typeof memberId == "undefined" || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    if (typeof money == "undefined" || money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }
    var item = [{
            name: "Sepatu Stacattu",
            price: 1500000
        },
        {
            name: "Baju Zoro",
            price: 500000
        },
        {
            name: "Baju H&N",
            price: 250000
        },
        {
            name: "Sweater Uniklooh",
            price: 175000
        },
        {
            name: "Casing Handphone",
            price: 50000
        }
    ];
    var data = {};
    data.memberId = memberId;
    data.money = money;

    var listPurchased = [];
    var changeMoney = 0;
    for (var i = 0; i < item.length; i++) {
        if (money >= item[i]["price"]) {
            listPurchased.push(item[i]["name"]);
            money -= item[i]["price"];
        }
        changeMoney = money;
    }
    data.listPurchased = listPurchased;
    data.changeMoney = changeMoney;
    return data;
}


//shoppingTime('abcd', 2475000);
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


function naikAngkot(arrPenumpang) {

    var arraydata = [];
    if (arrPenumpang.length > 0) {
        rute = ['A', 'B', 'C', 'D', 'E', 'F'];

        for (var hitung = 0; hitung < arrPenumpang.length; hitung++) {
            var objectdata = {};
            objectdata.penumpang = arrPenumpang[hitung][0];
            objectdata.naikDari = arrPenumpang[hitung][1];
            objectdata.tujuan = arrPenumpang[hitung][2];
            var diff = rute.indexOf(objectdata.tujuan) - rute.indexOf(objectdata.naikDari);
            objectdata.bayar = diff * 2000;
            arraydata.push(objectdata);
        }
    }
    return arraydata;

    //your code here
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]