//soal A
function DescendingTen(num) {
    var hasil = "";
    var i = 1;
    if (typeof (num) === "undefined")
        return -1;
    do {
        hasil += num + " ";
        num -= 1;
        i++;
    }
    while (i <= 10);

    return hasil;
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


//soal B
function AscendingTen(num) {
    var hasil = "";
    var i = 1;
    if (typeof (num) === "undefined")
        return -1;
    do {
        hasil += num + " ";
        num += 1;
        i++;
    }
    while (i <= 10);

    return hasil;
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1


//Soal C
function ConditionalAscDesc(reference, check) {
    var hasil;
    if (typeof (reference) === "undefined" || typeof (check) === "undefined")
        return -1;
    if (check % 2 == 1)
        return AscendingTen(reference);
    else
        return DescendingTen(reference);
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1