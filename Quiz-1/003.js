function AscendingTen(num) {
    var hasil = "";
    var i = 1;
    if (typeof (num) === "undefined")
        return -1;
    do {
        hasil += num + " ";
        num += 1;
        i++;
    }
    while (i <= 10);

    return hasil;
}

function DescendingTen(num) {
    var hasil = "";
    var i = 1;
    if (typeof (num) === "undefined")
        return -1;
    do {
        hasil += num + " ";
        num -= 1;
        i++;
    }
    while (i <= 10);

    return hasil;
}

function ularTangga() {
    var num = 100;
    var hasil = "";
    for (i = 1; i <= 10; i++) {
        if (i % 2 == 1) {
            hasil += DescendingTen(num);
            num -= 9;
        } else {
            hasil += AscendingTen(num);
            num += 9;
        }
        num = num - 10;
        hasil += "\n";
    }
    return hasil;
}

console.log(ularTangga());