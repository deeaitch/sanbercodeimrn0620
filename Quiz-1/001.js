//soal A
function terbalik(input) {
    var kalimat = "";
    for (var i = input.length - 1; i >= 0; i--) {
        kalimat += input[i];
    }
    return kalimat;
}

console.log(terbalik("abcde")) // edcba
console.log(terbalik("rusak")) // kasur
console.log(terbalik("racecar")) // racecar
console.log(terbalik("haji")) // ijah


//soal B
function maksimum(num1, num2) {
    if ((typeof (num1) === "undefined" && typeof (num2) === "undefined") || num1 == num2 || num1 < 0 || num2 < 0)
        return -1;
    else if (typeof (num2) === "undefined")
        return num1;
    else
        return (num1 < num2 ? num2 : num1);
}

console.log(maksimum(10, 15)); // 15
console.log(maksimum(12, 12)); // -1
console.log(maksimum(-1, 10)); // -1 
console.log(maksimum(112, 121)); // 121
console.log(maksimum(1)); // 1
console.log(maksimum()); // -1
console.log(maksimum("15", "18")) // 18


//soal C
function palindrome(kalimat) {
    return (kalimat == terbalik(kalimat) ? true : false);
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false