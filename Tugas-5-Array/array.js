//soal no 1
function range(startNum, finishNum) {
    var number = [];
    if ((typeof (startNum) === "undefined" || typeof (finishNum) === "undefined"))
        return -1;
    else if (finishNum < startNum) {
        for (i = startNum; i >= finishNum; i--) {
            number.push(i);
        }
    } else {
        for (i = startNum; i <= finishNum; i++) {
            number.push(i);
        }
    }

    return number;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//soal no 2
function rangeWithStep(startNum, finishNum, step) {
    var number = [];
    if ((typeof (startNum) === "undefined" || typeof (finishNum) === "undefined" || typeof (step) === "undefined"))
        return -1;
    else if (finishNum < startNum) {
        for (i = startNum; i >= finishNum; i -= step) {
            number.push(i);
        }
    } else {
        for (i = startNum; i <= finishNum; i += step) {
            number.push(i);
        }
    }

    return number;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//soal no 3
function sum(startNum, finishNum, step) {
    var total = 0;
    var steps = (typeof (step) === "undefined" ? 1 : step)
    if ((typeof (startNum) === "undefined" && typeof (finishNum) === "undefined"))
        return 0;
    else if ((typeof (startNum) === "undefined" || typeof (finishNum) === "undefined"))
        return 1;
    else if (finishNum < startNum) {
        for (i = startNum; i >= finishNum; i -= steps) {
            total += i;
        }
    } else {
        for (i = startNum; i <= finishNum; i += steps) {
            total += i;
        }
    }

    return total;
}


console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//soal no 4
function dataHandling(daftar) {
    var kalimat = "";
    for (var i = 0; i < daftar.length; i++) {
        for (var j = 0; j <= 4; j++) {
            switch (j) {
                case 0:
                    kalimat += "Nomor ID : ";
                    break;
                case 1:
                    kalimat += "Nama Lengkap : ";
                    break;
                case 2:
                    kalimat += "TTL : ";
                    break;
                case 4:
                    kalimat += "Hobi : "
                    break;
                default:
                    break;
            }
            kalimat += (j == 2 ? daftar[i][j] + " " : daftar[i][j] + "\n");
        }
        kalimat += "\n";
    }
    return kalimat;
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input));

//soal no 5
function balikKata(input) {
    var kalimat = "";
    for (var i = input.length - 1; i >= 0; i--) {
        kalimat += input[i];
    }
    return kalimat;
}


console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


//soal no 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");

input.splice(4, 1, "Pria", "SMA Internasional Metro");

console.log(input);

hasiltgl = input[3].split("/");
switch (hasiltgl[1]) {
    case "01": {
        console.log(" Januari ");
        break;
    }
    case "02": {
        console.log(" Februari ");
        break;
    }
    case "03": {
        console.log(" Maret ");
        break;
    }
    case "04": {
        console.log(" April ");
        break;
    }
    case "05": {
        console.log(" Mei ");
        break;
    }
    case "06": {
        console.log(" Juni ");
        break;
    }
    case "07": {
        console.log(" Juli ");
        break;
    }
    case "08": {
        console.log(" Agustus ");
        break;
    }
    case "09": {
        console.log(" September ");
        break;
    }
    case "10": {
        console.log(" Oktober ");
        break;
    }
    case "11": {
        console.log(" November ");
        break;
    }
    case "12": {
        console.log(" Desember ");
        break;
    }
    default: {
        console.log("Tidak ada bulan dengan angka itu");
        break;
    }
}
joinan = hasiltgl.join("-");
console.log(joinan);

sortir = hasiltgl.sort();
console.log(sortir);

console.log(input[1].slice(0, 14));